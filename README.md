# Journal d'une installation d'un serveur Dédié

## Commande du serveur

... mettre détails ici

## Installation

Choix de la distribution Debian GNU/Linux 12

Partitionnement

Mots de passes et clef publique SSH

Lancement de l'installation : 04/12/2023, 13h35

13h57 : done, il faut attendre une heure avant de se connecter (???)

~~~~Bash
jpx11@edinburgh:~/local2/mloukhiya$ ssh jpierre@51.159.29.46
The authenticity of host '51.159.29.46 (51.159.29.46)' can't be established.
ED25519 key fingerprint is SHA256:xiAjHGBaNksrgF3Btq6m5vu+qytboPB/eEwaK6Vyg64.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '51.159.29.46' (ED25519) to the list of known hosts.
Linux mloukhiya 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
jpierre@mloukhiya:~$ uptime
uptime
 13:58:59 up 2 min,  1 user,  load average: 0.17, 0.14, 0.05
jpierre@mloukhiya:~$ uname -a
Linux mloukhiya 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64 GNU/Linux
root@mloukhiya:~# lsblk --fs
NAME   FSTYPE FSVER LABEL UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
sda                                                                           
├─sda1 ext4   1.0         46e3d9b8-f881-4d4c-ad39-2b48de19d059  873.2M     6% /boot
├─sda2 ext4   1.0         d84e2012-e103-4c38-9849-2346350a4ecd  215.5G     1% /
└─sda3 swap   1           bf88e509-5a9e-4ab5-9e22-f695b1e2f23e                [SWAP]
root@mloukhiya:~# df -h
Filesystem      Size  Used Avail Use% Mounted on
udev            1.9G     0  1.9G   0% /dev
tmpfs           392M  424K  391M   1% /run
/dev/sda2       229G  1.5G  216G   1% /
tmpfs           2.0G     0  2.0G   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/sda1       997M   56M  874M   6% /boot
tmpfs           392M     0  392M   0% /run/user/1000
~~~~

Étapes suivante :

su -
apt update
apt upgrade (rien à faire)
apt install sudo vim
usermod -a -G sudo jpierre

Examinons la conf réseau :

~~~~Bash
# ip a
...
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:08:a2:0c:84:b6 brd ff:ff:ff:ff:ff:ff
    inet 51.159.29.46/24 brd 51.159.29.255 scope global enp1s0
       valid_lft forever preferred_lft forever
    inet6 fe80::208:a2ff:fe0c:84b6/64 scope link 
       valid_lft forever preferred_lft forever
~~~~~ 

IPv4 publique ! Pas encore V6, ça va venir !

sshd_config : PasswordAuthentication no et PermitRootLogin no
(c'est normalement le défaut mais bon) et restart ssh

Roadmap 

- DNS
- Mail
- Proxy Web nginx
- let's encrypt
- Docker et/ou LXC (LXD ?) : avec Terraform !
- DB
- Contenu Web
- OpenVPN

IPv6 en interne ? et pour le lien avec le Cloud ?
Dual stack pour commencer (au cas où on plante le v6)
Ensuite on verra...

## Ouf !

~~~~Bash
# update-alternatives --config editor
There are 3 choices for the alternative editor (providing /usr/bin/editor).

  Selection    Path                Priority   Status
------------------------------------------------------------
* 0            /bin/nano            40        auto mode
  1            /bin/nano            40        manual mode
  2            /usr/bin/vim.basic   30        manual mode
  3            /usr/bin/vim.tiny    15        manual mode

Press <enter> to keep the current choice[*], or type selection number: 2
update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/editor (editor) in manual mode
~~~~

ssh-keygen en root et git pour /.etc.git/

bash_completion
PS1 root
psutils psmisc

~~~~Bash
$ pstree
systemd─┬─agetty
        ├─cron
        ├─dbus-daemon
        ├─dhclient
        ├─named───5*[{named}]
        ├─ntpd───ntpd───ntpd
        ├─sshd───sshd───sshd───bash───sudo───sudo───bash───pstree
        ├─systemd───(sd-pam)
        ├─systemd-journal
        ├─systemd-logind
        └─systemd-udevd
~~~~

Go for containerd : apt install containerd (discuss pourquoi, versions, etc.)

quelle registry utilisé ? ou pas du tout ?

fail2ban (sshd) => rsyslog + iptables

8 juillet 2023 : première màj noyau (sécu), reboot.

Ajout clef locale (jpierre@mloukhiya) dans Gitlab

https://community.openvpn.net/openvpn/wiki/OpenvpnSoftwareRepos#DebianUbuntu:UsingOpenVPNaptrepositories

+ le script habituel (curl etc)

arf : sudo chmod go=rx /home/*

just in case, maybe : openvpn --config client.ovpn --auth-retry interact --log /var/log/openvpn.log

à tester pour log de conn passées, pas sûr que nécessaire

## NGINX et HTTPS (Let's Encrypt)

Après avoir bien renseigné les noms du serveur HTTP dans /etc/nginx/sites-available/default

~~~~Bash
sudo apt install certbot python3-certbot-nginx
sudo systemctl restart nginx
sudo certbot --nginx -d noedge.net -d www.noedge.net
sudo systemctl status certbot.timer 
~~~~

Et c'est tout !

Pour surveiller les logs sans avoir besoin de sudo : `usermod -a -G adm jpierre`
